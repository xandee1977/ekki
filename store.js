const Store = require('openrecord/store/postgres')

const store = new Store({
  type: 'postgres',
  host: 'localhost',
  database: 'postgres',
  user: 'ekki',
  password: 'ekki',
  autoLoad: true
})

store.ready(async () => {
  const user = await store.Model('User').find(1)
  console.log(user)
})