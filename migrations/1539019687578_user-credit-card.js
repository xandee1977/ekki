exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("cards", {
    card_id : "id",
    user_id : {
      type: "integer",
      notNull: true,
      references: '"users"',
      onDelete: "cascade"
    },    
    card_number     : { type: "integer"},
    card_name 		: { type: "varchar(1000)"},
    card_expire_month   : { type: "integer"},
    card_expire_year    : { type: "integer"},
    card_security   : { type: "integer"},
    card_date       : {
        type: "timestamp", 
        notNull: true, 
        default: pgm.func("current_timestamp") 
    }
  });

  pgm.createIndex("cards", "user_id");
};

exports.down = (pgm) => {
    pgm.dropTable("cards");
};