exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("contacts", {
    user_id : {
      type: "integer",
      notNull: true,
      references: '"users"',
      onDelete: "cascade"
    },
    contact_id : {
      type: "integer",
      notNull: true,
      references: '"users"',
      onDelete: "cascade"
    }    
  });
  pgm.createIndex("contacts", "user_id");
  pgm.createIndex("contacts", "contact_id");
};

exports.down = (pgm) => {
    pgm.dropTable("contacts");
};