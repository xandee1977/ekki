exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("users", {
    user_id         : "id",
    user_first_name :  { type: "varchar(1000)"},
    user_last_name  :  { type: "varchar(1000)"},
    user_nickname   :  { type: "varchar(1000)"},
    user_passwd     :  { type: "varchar(1000)"},
    user_agency 	:  { type: "integer"},
    user_account 	:  { type: "integer"},
    user_date       :  { type: "timestamp", notNull: true, default: pgm.func("current_timestamp") }
  });
};

exports.down = (pgm) => {
    pgm.dropTable("users");
};
