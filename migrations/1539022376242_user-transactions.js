exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("transactions", {
  	transaction_id : "id",
  	origin_id: {
      type: "integer",
      notNull: true,
      references: '"users"',
      onDelete: "cascade"
    },
  	destiny_id: {
      type: "integer",
      notNull: true,
      references: '"users"',
      onDelete: "cascade"
    },
    value : { type: "float"},
    description : { type: "varchar(1000)"},
    timestamp : {
        type: "timestamp", 
        notNull: true, 
        default: pgm.func("current_timestamp") 
    }
  });

  pgm.createIndex("transactions", "origin_id");
  pgm.createIndex("transactions", "destiny_id");
};

exports.down = (pgm) => {
    pgm.dropTable("transactions");
};