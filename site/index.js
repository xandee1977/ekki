var express     = require('express');
var nunjucks    = require('nunjucks');
var dateFormat  = require('dateformat');
var bodyParser  = require('body-parser');
const uuid      = require('uuid/v4')
var session     = require('express-session')


// Server configs
app = express();
port = process.env.PORT || 3000;
app.listen(port);

// Static files
app.use(express.static('public'));

// Defining body parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Template configs
nunjucks.configure('views', {
  autoescape: true,
  express: app
});

// Configure Session Middleware
app.use(
  session({
    key: 'user_sid',
    // Session fingerprint
    secret: '387dadad-9abd-4cb5-a9fe-503fe77e89ed',
    resave: false,
    saveUninitialized: false,
      cookie: {
          expires: 600000
      }
    }
  )
);

/* Routing */

/* APP */
// Index
app.get('/', function(req, res) {
  if( ! req.session.user_id ) {
    res.redirect('/login');
  } else {
    res.redirect('/home');
  }
});

// Loads login form
app.get('/login', function(req, res) {
  res.render('login.html');
});

// Logout route
app.get('/logout', function(req, res) {
  req.session.destroy();
  res.redirect('/login');
});

// Post login data
app.post('/login', function(req, res) {
  let User = require('./models/user.js');
  let username = req.body.username;
  let password = req.body.password;

  var user = new User();
  user.load( username, password )
    .then(( resultSet ) => {
        if ( resultSet.rows.length == 0 ) {
          //console.log('User not found!');
          res.redirect('/');
        }

        let data = resultSet.rows[0];
        req.session.user_id = data.user_id;
        res.redirect('/home');
    })
    .catch(( err ) => { 
        //console.log("Error!");
        //console.log(err);
        res.redirect('/');
    });
});

app.get('/home', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }
  res.render('index.html', { user_id : req.session.user_id });
});

/* CARD */
// Save
app.post('/save-card', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }

  let user_id = req.session.user_id;
  let Card = require('./models/card.js');
  let cardData    = req.body.card

  var card = new Card();
  card.inserir( user_id, cardData )
    .then(( resultSet ) => {
        req.session.alertMessage = "Cartão incluído com sucesso!"
        res.redirect('/cards');
    })
    .catch(( err ) => { 
        //console.log(err);
        res.redirect('/cards');
    });
});

app.get('/card-remove/:card_id', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }
  
  let card_id = req.params.card_id;

  let Card = require('./models/card.js');
  var card = new Card();
  
  card.remove( card_id )
    .then(( resultSet ) => {
      req.session.alertMessage = "Cartão excluido com sucesso!";
      res.redirect('/cards');
    })
    .catch(( err ) => { 
      res.redirect('/cards');
    });
});

// list
app.get('/cards', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }

  let user_id = req.session.user_id;
  let Card = require('./models/card.js');
  let cards = [];

  // Alert Message
  var alertMessage = req.session.alertMessage;
  delete req.session['alertMessage'];

  var card = new Card();
  card.list( user_id )
    .then(( resultSet ) => {
        resultSet.rows.forEach(function(row) {
          cards.push(row);
        });

        res.render('cards.html', Object.assign({cards: cards, alertMessage: alertMessage }, req.session))
    })
    .catch(( err ) => { 
        //console.log(err);        
        res.render('cards.html', Object.assign({cards: [], alertMessage: alertMessage}, req.session))
    });
});

// History
app.get('/history', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }

  let user_id = req.session.user_id;
  let Transaction = require('./models/transaction.js');
  let history = [];
  
  // Alert Message
  var alertMessage = req.session.alertMessage;
  delete req.session['alertMessage'];

  var transaction = new Transaction();
  transaction.list( user_id )
    .then(( resultSet ) => {
        resultSet.rows.forEach(function(row) {
          row.date = dateFormat(
            new Date(row.timestamp), 
            "dd/mm/yyyy - hh:MM"
          );
          
          history.push(row);
        });
        
        res.render('history.html', Object.assign({history: history, alertMessage: alertMessage}, req.session))
    })
    .catch((err) => { 
        //console.log(err);        
        res.render('history.html', Object.assign({history: [], alertMessage: alertMessage}, req.session))
    });    
});

// Contacts
app.get('/contacts', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }
    
  let user_id = req.session.user_id;
  let Contact = require('./models/contact.js');
  let contacts = [];

  // Alert Message
  var alertMessage = req.session.alertMessage;
  delete req.session['alertMessage'];

  var contact = new Contact();
  contact.list( user_id )
    .then(( resultSet ) => {
        resultSet.rows.forEach(function(row) {
          contacts.push(row);
        });
        
        res.render('contacts.html', Object.assign({contacts: contacts, alertMessage: alertMessage}, req.session))
    })
    .catch(( err ) => { 
        //console.log(err);        
        res.render('contacts.html', Object.assign({contacts: [], alertMessage: alertMessage}, req.session))
    });
});

app.get('/contact-add', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }

  res.render('contact-add.html');
});

app.get('/contact-save/:contact_user_id', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }
  
  // User and Contact ID
  let contact_user_id = req.params.contact_user_id;
  let user_id         = req.session.user_id;
  
  // Loads de model layer
  let Contact = require('./models/contact.js');
  var contact = new Contact();
  
  contact.add( user_id, contact_user_id )
    .then(( resultSet ) => {
      req.session.alertMessage = "Contato adicionado com sucesso!";
      res.redirect('/contacts');
    })
    .catch(( err ) => { 
      //console.log(err);
    });
});

app.get('/contact-remove/:contact_user_id', function(req, res) {
  // Check session
  if( ! req.session.user_id ) {
    res.redirect('/login');
    return;
  }
  
  // User and Contact ID
  let contact_user_id = req.params.contact_user_id;
  let user_id         = req.session.user_id;
  
  // Loads de model layer
  let Contact = require('./models/contact.js');
  var contact = new Contact();
  
  contact.remove( user_id, contact_user_id )
    .then(( resultSet ) => {
      req.session.alertMessage = "Contato excluido com sucesso!";
      res.redirect('/contacts');
    })
    .catch(( err ) => { 
      //console.log(err);
      res.redirect('/contacts');
    });
});

// Transference
app.post('/transference', function(req, res) {
  let Transaction = require('./models/transaction.js');
  let origin_id   = req.body.transference.origin_id;
  let destiny_id  = req.body.transference.destiny_id;
  let value       = req.body.transference.value;
  let description = req.body.transference.description;

  var transaction = new Transaction();
  transaction.transference( origin_id, destiny_id, value, description)
    .then(( resultSet ) => {
      req.session.alertMessage = 'Transferência realizada com sucesso!';
      res.redirect('/history');
    })
    .catch(( err ) => { 
      res.redirect('/history');
    });
});

/* API */
// List transactions
app.get('/balance/:user_id', function(req, res) {
  let user_id     = req.params.user_id;
  let Transaction = require('./models/transaction.js');
  var transaction = new Transaction();

  transaction.balance( user_id )
  .then((result) => {
    if( result.rows.length == 0 ) {
      res.json({balance: 0, message: 'Error on loading balance'});
      return;
    }
    res.json({balance: result.rows[0].balance, message: 'Success!'});
  })
  .catch((err) => { 
    res.json({balance: 0, message: err});
  });
});

// List contacts
app.get('/contacts/:user_id', function(req, res) {

  let user_id     = req.params.user_id;
  let Contact = require('./models/contact.js');
  let contacts = [];

  var contact = new Contact();
  contact.list( user_id )
    .then(( resultSet ) => {
        if( resultSet.rows.length == 0 ) {
          res.json({contacts: [], message: 'Error on loading contacts'});
          return;
        }

        resultSet.rows.forEach(function(row) {
          contacts.push(row);
        });

        res.json({contacts: contacts, message: 'Success!'});
    })
    .catch(( err ) => { 
        //console.log(err);        
        res.json({contacts: [], message: err});
        return;        
    });
});

// User Search by Account
app.get('/user-detail/:user_agency/:user_account', function(req, res) {
  let user_agency  = req.params.user_agency;
  let user_account = req.params.user_account;
  
  let User = require('./models/user.js');
  var user = new User();

  user.searchByAccount( user_agency, user_account )
    .then(( resultSet ) => {
        if( resultSet.rows.length == 0 ) {
          res.json({data: null, message: 'Error on loading user detail'});
          return;
        }
        res.json({data: resultSet.rows[0], message: 'Success!'});
    })
    .catch(( err ) => { 
        res.json({data: null, message: err});
        return;
    });
});

console.log('Server Started On: ' + port);