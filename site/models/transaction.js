// Database configuration
var DBConfig = require('../config/database.js');
const {Pool, Client} = require('pg')
const pool = new Pool(DBConfig)

class Transaction {
  // (Observable) list
  list( user_id ) {
    let query        = `
      SELECT 
        T.*,
        O.user_first_name as origin_name,
        O.user_last_name as origin_lname
      FROM 
        transactions as T LEFT JOIN
        users as O on T.origin_id = O.user_id
      WHERE 
        destiny_id=${user_id}
      ORDER BY
        timestamp DESC
    `;

    return pool.query( query );
  }

  balance( user_id ) {
    let query        = `
      SELECT 
        coalesce(sum(value), 0) as balance 
      FROM 
        transactions
      WHERE
        destiny_id=${user_id}
    `;
    return pool.query( query );
  }

  transference( origin_id, destiny_id, value, description='') {
    let query = `
      INSERT INTO 
        transactions(
          origin_id, 
          destiny_id, 
          value, 
          description
        )
      VALUES (
        ${origin_id}, 
        ${destiny_id}, 
        ${value}, 
        '${description}'
      );
      INSERT INTO 
        transactions(
          origin_id, 
          destiny_id, 
          value, 
          description
        )
      VALUES (
        ${destiny_id}, 
        ${origin_id}, 
        ${value * -1}, 
        '${description}'
      );
    `;
    
    return pool.query( query );
  }
}

module.exports = Transaction;