// Database configuration
var DBConfig = require('../config/database.js');
const {Pool, Client} = require('pg')
const pool = new Pool(DBConfig)

class User {
  
  // Load
  load( username, password ) {
    let query = `
      SELECT 
        * 
      FROM 
        users 
      WHERE
        user_nickname='${username}' AND 
        user_passwd='${password}';
    `;

    return pool.query( query );
  }

  // Searchs an user by it account
  searchByAccount( user_agency, user_account ) {
    let query = `
      SELECT 
        * 
      FROM 
        users 
      WHERE
        user_agency='${user_agency}' AND 
        user_account='${user_account}';
    `;

    return pool.query( query );    
  }
}

module.exports = User;