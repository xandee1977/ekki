// Database configuration
var DBConfig = require('../config/database.js');
const {Pool, Client} = require('pg')
const pool = new Pool(DBConfig)

class Card {
  // (Observable) list
  list( user_id ) {
    let query        = `
      SELECT 
        * 
      FROM 
        cards
      WHERE
        user_id=${user_id}
      ORDER BY
        card_id desc
    `;

    return pool.query( query );
  }

  inserir( user_id, card ) {
    let query        = `
      INSERT INTO
        cards(
          user_id, 
          card_number, 
          card_name, 
          card_expire_month, 
          card_expire_year, 
          card_security, 
          card_date
        )
        values(
          ${user_id}, 
          ${card.card_number}, 
          '${card.card_name}', 
          ${card.card_expire_month},  
          ${card.card_expire_year}, 
          ${card.card_security}, 
          NOW()
        )
    `;
    
    return pool.query( query );
  }

  remove( card_id ) {
    let query = `
      DELETE FROM 
        cards 
      WHERE 
        card_id=${card_id};
    `;
    
    return pool.query( query );    
  }  
}

module.exports = Card;