// Database configuration
var DBConfig = require('../config/database.js');
const {Pool, Client} = require('pg')
const pool = new Pool(DBConfig)

class Contact {
  
  // (Observable) list
  list( user_id ) {
    let query        = `
      SELECT 
        U.* 
      FROM 
        contacts as C left join
        users as U on C.contact_id=U.user_id
      WHERE
        C.user_id=${user_id}
      ORDER BY
        U.user_first_name ASC
    `;

    return pool.query( query );
  }

  add( user_id, contact_user_id ) {
    let query = `
      INSERT INTO 
        contacts(
          user_id, 
          contact_id
        )
        VALUES(
          ${user_id},
          ${contact_user_id}
        );
    `;
    
    return pool.query( query );    
  }

  remove( user_id, contact_user_id ) {
    let query = `
      DELETE FROM 
        contacts 
      WHERE 
        user_id=${user_id} AND 
        contact_id=${contact_user_id};
    `;

    return pool.query( query );    
  }  
}

module.exports = Contact;