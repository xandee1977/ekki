DATABASE README

1 - Access postgres from terminal
	psql -U postgres

2 - Create the App User:
	CREATE USER ekki WITH PASSWORD 'ekki';

3 - Create the App Database
	CREATE DATABASE ekki OWNER ekki;

4 - Exports the database variable
	export DATABASE_URL=postgres://ekki:ekki@localhost:5432/ekki

5 - Manage de migrations:
	- UP: npm run migrate up
	- DOWN: npm run migrate up